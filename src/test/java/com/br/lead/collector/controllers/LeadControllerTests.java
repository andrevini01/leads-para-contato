package com.br.lead.collector.controllers;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcResultMatchersDsl;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
public class LeadControllerTests {
    @MockBean
    LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Lead lead;
    Produto produto;
    Optional<Lead> leadOptionalMock;
    Iterable<Lead> leadIterableMock;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("email@testmock.com");
        lead.setNome("TesteMock");

        produto = new Produto();
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda");

        lead.setProdutos(Arrays.asList(produto));
        leadOptionalMock = Optional.of((Lead) lead);
        leadIterableMock = Arrays.asList(lead);
    }

    @Test
    public void testarInserirLead() throws Exception {
        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        Mockito.when(leadService.inserirLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtosIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads/inserirlead")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarTodosLeads() throws Exception{
        Mockito.when(leadService.buscarTodosLeads()).thenReturn(leadIterableMock);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("TesteMock")));

    }

    @Test
    public void testarBuscarLead() throws Exception{
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalMock);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo("email@testmock.com")));

    }

    @Test
    public void testarBuscarLeadNoContent() throws Exception{
        leadOptionalMock = Optional.empty();
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalMock);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarAtualizarLead() throws Exception{
        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].descricao", CoreMatchers.equalTo("Café da fazenda")));
    }

    @Test
    public void testarDeletarLead() throws Exception{
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalMock);
        Mockito.doNothing().when(leadService).deletarLead(Mockito.any(Lead.class));

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].descricao", CoreMatchers.equalTo("Café da fazenda")));
    }

    @Test
    public void testarDeletarLeadNoContent() throws Exception{
        leadOptionalMock = Optional.empty();
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalMock);
        Mockito.doNothing().when(leadService).deletarLead(Mockito.any(Lead.class));

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}