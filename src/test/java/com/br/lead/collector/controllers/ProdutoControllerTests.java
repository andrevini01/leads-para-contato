package com.br.lead.collector.controllers;

import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.swing.text.html.Option;
import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTests {

    @MockBean
    ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Produto produto;
    Optional<Produto> produtoOptionalMock;
    Iterable<Produto> produtoIterableMock;

    @BeforeEach
    public void inicializar(){
        produto = new Produto();
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda");

        produtoIterableMock = Arrays.asList(produto);
        produtoOptionalMock = Optional.of((Produto) produto);
    }

    @Test
    public void testarBuscarProduto() throws Exception{
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptionalMock);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Café")));
    }

    @Test
    public void testarBuscarProdutoNoContent() throws Exception{
        produtoOptionalMock = Optional.empty();
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptionalMock);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarBuscarTodosProduto() throws Exception{
        Mockito.when(produtoService.buscarTodosProdutos()).thenReturn(produtoIterableMock);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].preco", CoreMatchers.equalTo(10.0)));
    }

    @Test
    public void testarInserirProduto() throws Exception{
        Mockito.when(produtoService.inserirProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos/inserirproduto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Café")));
    }

    @Test
    public void testarAtualizarProduto() throws Exception{
        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo("Café da fazenda")));
    }

    @Test
    public void testarDeletarProduto() throws Exception{
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptionalMock);
        Mockito.doNothing().when(produtoService).deletarProduto(Mockito.any(Produto.class));

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco", CoreMatchers.equalTo(10.0)));
    }

    @Test
    public void testarDeletarProdutoNoContent() throws Exception{
        produtoOptionalMock = Optional.empty();
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptionalMock);
        Mockito.doNothing().when(produtoService).deletarProduto(Mockito.any(Produto.class));

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
