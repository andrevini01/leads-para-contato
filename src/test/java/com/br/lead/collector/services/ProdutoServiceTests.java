package com.br.lead.collector.services;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTests {
    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    Produto produto;
    Optional<Produto> produtoOptionalMock;
    Iterable<Produto> produtoIterableMock;

    @BeforeEach
    public void inicializar(){
        produto = new Produto();
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda");

        produtoIterableMock = Arrays.asList(produto);
        produtoOptionalMock = Optional.of((Produto) produto);
    }

    @Test
    public void testarBuscarPorId(){
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptionalMock);

        Optional<Produto> produtoOptionalRetorno = produtoService.buscarPorId(1);

        Assertions.assertEquals(produtoOptionalRetorno, produtoOptionalMock);
        Assertions.assertEquals(produtoOptionalRetorno.get().getNome(), "Café");
    }

    @Test
    public void testarBuscarTodosProdutos(){
        Mockito.when(produtoRepository.findAll()).thenReturn(produtoIterableMock);

        Iterable<Produto> produtoIterableRetorno = produtoService.buscarTodosProdutos();

        Assertions.assertEquals(produtoIterableRetorno, produtoIterableMock);
        Assertions.assertEquals(produtoIterableRetorno.iterator().next().getNome(), "Café");
    }

    @Test
    public void testarInserirProduto(){
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoObjeto = produtoService.inserirProduto(produto);

        Assertions.assertEquals(produto, produtoObjeto);
        Assertions.assertEquals(produto.getDescricao(), produtoObjeto.getDescricao());
    }

    @Test
    public void testarAtualizarProduto(){
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptionalMock);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoRetorno = produtoService.atualizarProduto(produto);

        Assertions.assertEquals(produtoRetorno, produto);
        Assertions.assertEquals(produtoRetorno.getNome(), produto.getNome());
    }

    @Test
    public void testarAtualizarProdutoPassandoApenasId(){
        produto = new Produto();
        produto.setId(1);

        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptionalMock);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoRetorno = produtoService.atualizarProduto(produto);

        Assertions.assertEquals(produtoRetorno, produto);
        Assertions.assertEquals(produtoRetorno.getNome(), produto.getNome());
    }

    @Test
    public void testarDeletarProduto(){
        produtoService.deletarProduto(produto);
        Mockito.verify(produtoRepository, Mockito.times(1)).delete(Mockito.any(Produto.class));
    }
}
