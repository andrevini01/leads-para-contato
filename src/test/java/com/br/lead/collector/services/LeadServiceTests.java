package com.br.lead.collector.services;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.LeadRepository;
import com.br.lead.collector.repositories.ProdutoRepository;
import com.sun.xml.bind.v2.model.core.ID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.configuration.IMockitoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.persistence.Id;
import java.util.*;

@SpringBootTest
public class LeadServiceTests {

    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    Lead lead;
    Produto produto;
    Optional<Lead> leadOptionalMock;
    Iterable<Produto> produtoIterableMock;
    List<Integer> listaId;
    Iterable<Lead> leadIterableMock;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("email@testmock.com");
        lead.setNome("TesteMock");
        lead.setProdutos(Arrays.asList(new Produto()));

        produto = new Produto();
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda");

        leadOptionalMock = Optional.of((Lead) lead);
        produtoIterableMock = Arrays.asList(produto);

        listaId = new ArrayList<>();
        listaId.add(1);

        leadIterableMock = Arrays.asList(lead);
    }

    @Test
    public void testarInserirLead(){
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadService.inserirLead(lead);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
    }

    @Test
    public void testarBuscarTodosProdutos(){
        Mockito.when(produtoRepository.findAllById(Mockito.anyList())).thenReturn(produtoIterableMock);

        Iterable<Produto> produtoIterableRetorno = leadService.buscarTodosProdutos(listaId);

        Assertions.assertEquals(produtoIterableRetorno, produtoIterableMock);
        Assertions.assertEquals(produtoIterableRetorno.iterator().next().getNome(), "Café");
    }

    @Test
    public void testarBuscarPorId(){
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptionalMock);

        Optional<Lead> leadOptionalRetorno = leadService.buscarPorId(1);

        Assertions.assertEquals(leadOptionalRetorno, leadOptionalMock);
        Assertions.assertEquals(leadOptionalRetorno.get().getNome(), "TesteMock");
    }

    @Test
    public void testarBuscarTodosLeads(){
        Mockito.when(leadRepository.findAll()).thenReturn(leadIterableMock);

        Iterable<Lead> leadIterableRetorno = leadService.buscarTodosLeads();

        Assertions.assertEquals(leadIterableRetorno, leadIterableMock);
        Assertions.assertEquals(leadIterableRetorno.iterator().next().getNome(), "TesteMock");
    }

    @Test
    public void testarAtualizarLead(){
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalMock);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadRetorno = leadService.atualizarLead(lead);

        Assertions.assertEquals(leadRetorno, lead);
        Assertions.assertEquals(leadRetorno.getNome(), lead.getNome());
    }

    @Test
    public void testarAtualizarLeadPassandoApenasProdutos(){
        lead = new Lead();
        lead.setId(1);
        lead.setProdutos(Arrays.asList(new Produto()));

        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalMock);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadRetorno = leadService.atualizarLead(lead);

        Assertions.assertEquals(leadRetorno, lead);
        Assertions.assertEquals(leadRetorno.getNome(), lead.getNome());
    }

    @Test
    public void testarDeletarLead(){
        leadService.deletarLead(lead);
        Mockito.verify(leadRepository, Mockito.times(1)).delete(Mockito.any(Lead.class));
    }
}





















