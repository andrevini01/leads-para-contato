package com.br.lead.collector.models;

import com.br.lead.collector.enums.TipoDeLead;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nome_completo")
    @Size(min = 8, max = 100, message = "O nome deve ter no mínimo 8 caracteres e máximo de 100.")
    private String nome;

    @Email(message = "O formato do email é inválido!")
    private String email;

    private TipoDeLead tipoDeLead;

    @ManyToMany
    private List<Produto> produtos;

    public Lead(){
    }

    public Lead(String nome, String email, TipoDeLead tipoLead) {
        this.nome = nome;
        this.email = email;
        this.tipoDeLead = tipoLead;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoDeLead getTipoDeLead() {
        return tipoDeLead;
    }

    public void setTipoDeLead(TipoDeLead tipoDeLead) {
        this.tipoDeLead = tipoDeLead;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
